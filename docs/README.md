# Documentación SmartDevOps

En este repositorio se documentará la plataforma creada para el proyecto SmartDevOps.

## Requisitos de la documentación

```bash
pip3 install -r requirements.txt
```

## Construcción en local

```bash
mkdocs serve
```