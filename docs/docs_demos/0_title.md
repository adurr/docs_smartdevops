---
title: "SmartDevOps Demonstration Documentation"
author: [Universidad Politécnica de Madrid]
date: "12-04-2021"
subject: "Markdown"
keywords: [DevOps, Big Data]
#subtitle: "Subtítulo"
lang: "es"
titlepage: true
book: false

pandoc-latex-admonition:
  - color: firebrick
    classes: [admonition, danger]
    position: inner
    linewidth: 4
    nobreak: true
  - classes: [admonition]
    nobreak: true
...