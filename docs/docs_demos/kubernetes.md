# Kubernetes CheatSheet

## Services
- Create a service that exposes a deployment with an external IP:
```bash
kubectl expose deployment hello-world --type=LoadBalancer --name=my-service
```