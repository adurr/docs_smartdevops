# DevDocOps

[Material for Mkdocs](https://github.com/squidfunk/mkdocs-material) has been chosen because of its simplicity to transform the documentation to a pdf through pandoc.

```
pandoc -N --template=eisvogel.latex --listings -V lang=en --from markdown --variable mainfont="DejaVuSerif" --variable sansfont="DejaVuSans" --variable monofont="DejaVuSansMono" --variable fontsize=12pt --variable version=2.0 docs/0_title.md /home/devops/Documents/Doctorado/SmartDevOps/docs_SmartDevOps/site/docs.pdf.md  --pdf-engine=xelatex --toc -o docs_SmartDevOps.pdf
```