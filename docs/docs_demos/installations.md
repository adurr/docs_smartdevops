# Getting started

The following steps have been performed for the installation of the project demo.

## Helm installation

This installation is done following [Helm's documentation](https://helm.sh/docs/intro/install/).

## Stack Storm installation

- Using Helm:
```bash
helm repo add stackstorm https://helm.stackstorm.com/
helm install st2 stackstorm/stackstorm-ha
```
Creating a service in the `st2-svc.yaml` file of type LoadBalancer:
```yaml
apiVersion: v1
kind: Service
metadata:  
  name: st2-service  
  namespace: default
spec:
  type: LoadBalancer  
  ports:    
    - port: 80      
      targetPort: 80      
      protocol: TCP  
  selector:    
    app: st2web
```
Running `st2-svc.yaml` service:
```bash
kubectl apply -f st2-service
```
Getting the external-IP:
```bash
kubectl get svc st2-service
```

## Argo CD installation

- Create namespace and deployment:
```bash
kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
```
- Service creation:
```bash
kubectl patch svc argocd-server -n argocd -p '{"spec": {"type": "LoadBalancer"}}'
```
- Getting the password:
```bash
kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d
```

## Gitea installation
Using Helm:
```bash
helm repo add gitea-charts https://dl.gitea.io/charts/
helm install gitea gitea-charts/gitea
```
>  Error with PV