# Introduction

## What is SmartDevOps?

SmartDevOps is a project that seeks to simplify and automate the process of integrating tools in Big Data projects. It combines the flexibility of OSLC semantic standards with the power of an event-driven rules engine. It also offers an entire ecosystem for DevOps teams to manage the project, including a monitoring system and a ChatOps tool.

