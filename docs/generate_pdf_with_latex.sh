#!/bin/sh

pandoc -N --template=eisvogel.latex --listings -V lang=en --from markdown --variable mainfont="DejaVuSerif" --variable sansfont="DejaVuSans" --variable monofont="DejaVuSansMono" --variable fontsize=12pt --variable version=2.0 docs_demos/0_title.md site/docs.pdf.md  --pdf-engine=xelatex --toc -o docs_SmartDevOps.pdf